# React Native Test

Revision 1 (June 2021)

# References

1. Figma Screens - https://www.figma.com/file/ErBq90H0VmTy9fyq2z2bku/Developer-Test

# Test Requirements

1. Please refer to the Figma Screens to view the full flow.
2. Please push your answers into a public repository and send the URL to your recruiter. Your recruiter will forward it to the test reviewers.
3. Please add a `README.md` and include the following information:
   1. Dependency Manager used - for example, `yarn`, `npm` or something else.
   2. Instructions on starting the application if it is not `react-native start`.
   3. In case any of the instructions are not clear and you had to make assumptions, let us know about it here.
4. This is a test. We do not expect things to be pixel perfect. You can use components you are familiar with. Just let us know what you have done, as in requirement 3.3 above.

## Main Profile Screen

To implement an editable profile screen.

<img src="images/profile.png" width="250" />

The profile screen can be split into two sections:
* Profile data
* Industry data

Clicking on `Edit Profile` allows the user to change the profile data.

Clicking on `Edit Industry` allows the user to change the industry data.

**No changing of Avatar is required for the test.**

A user can click on both `Edit` buttons which means the both data sections will be in edit mode.

Each section will have its own `Save Changes` button, which are independent of each other. This means clicking the button below the `Profile data` section will only save the changes made to the `Profile data section`.

## Industry Popup

<img src="images/industrypopup.png" width="250" />

In the above screen, the values:
* UI/UX design
* Marketing
* Product Management
* Software Development

are meant to be auto-completion options. You do not need to implement any auto-completion.

Additionally `+ Add UX as new industry` is a string template, where `UX` is what the user has typed in the search box. If user types 'Marketing', the string should be shown as `+ Add Marketing as new industry`.

# End
